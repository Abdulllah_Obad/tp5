package fr.umfds.exo2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class JeuDeCartsTest {


    @InjectMocks
    JeuDeCartes jc  ;



    @Mock
    Random mockrandom;



    @Test
    void Testpioche() throws CarteIncorrecteException {

        when(mockrandom.nextInt(anyInt())).thenReturn(51);
        Carte carte_pioche=jc.pioche();
        Carte carte_cible= new Carte(13,Couleur.trefle);
        carte_pioche.equals(carte_cible);
    }

    @Test
    void Testpioche2() throws CarteIncorrecteException {

        when(mockrandom.nextInt(anyInt())).thenReturn(0);
        Carte c=jc.pioche();
        Carte carte= new Carte(1,Couleur.trefle);
        c.equals(carte);
    }
// Test A 100% (En plus ): test exception

    @Test
    void TestCarteNonValide(){
        assertThrows(CarteIncorrecteException.class,
                (()-> new Carte(17,Couleur.pique)) );
    }


    @Test
    void Testdistribution() throws CarteIncorrecteException, Exception {


        ArrayList<ArrayDeque<Carte>> listDeckRef=new ArrayList<>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();

        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(2, Couleur.pique));
        deck1.addLast(new Carte(3, Couleur.pique));

        deck2.addLast(new Carte(13, Couleur.trefle));
        deck2.addLast(new Carte(12, Couleur.trefle));
        deck2.addLast(new Carte(11, Couleur.trefle));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockrandom.nextInt(anyInt())).thenReturn(0, 0, 0, 48, 47, 46);

        ArrayList<ArrayDeque<Carte>> result = jc.distribution(3, 2);
        for(int i = 0; i < 2; i++)
            for(int j = 0; j < 3; j++)
                assertEquals(true, listDeckRef.get(i).pop().equals(result.get(i).pop()));


    }





/*
    @Test
    void pioche()  {
         jc = new JeuDeCartes();
         int i = 0;
         int     cible_h= 51;
         Couleur cible_c =Couleur.trefle;
         carte_Pioche = jc.pioche();
         while (carte_Pioche.getHauteur()!=cible_h&& carte_Pioche.getCouleur()!=cible_c){

         i=i+1;
         }
        assertEquals(carte_Pioche.getHauteur(),cible_h);
        assertEquals(carte_Pioche.getCouleur(),cible_c);

    }


    @Test
    void pioche2() throws CarteIncorrecteException {
        carte = new Carte(1,Couleur.trefle);
        jc = new JeuDeCartes();
        int i = 0;
        while (i<=51){
            carte = jc.pioche();
            assertEquals(carte.getHauteur(),carte.getHauteur());
            assertEquals(carte.getCouleur(),carte.getCouleur());
            i=i+1;
        }

    }

*/

}