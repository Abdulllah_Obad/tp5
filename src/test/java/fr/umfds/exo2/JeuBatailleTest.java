package fr.umfds.exo2;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)

class JeuBatailleTest {



  //  @InjectMocks
    //JeuBataille jb;

    @Mock
    JeuDeCartes jdc;


    @Test
    void round_2cartes() throws Exception, CarteIncorrecteException {

        ArrayDeque<Carte> paquet = new ArrayDeque<>();
        paquet.addLast(new Carte(12,Couleur.pique));
        paquet.addLast(new Carte(1,Couleur.coeur));

        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();
        paquet2.addLast(new Carte(12,Couleur.pique));
        paquet2.addLast(new Carte(3,Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet);
        paquets.add(paquet2);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        JeuBataille partie = new JeuBataille(jdc);

        assertTrue(partie.round());

    }
    @Test
    void round_1cartes() throws Exception, CarteIncorrecteException {

        ArrayDeque<Carte> paquet = new ArrayDeque<>();
        paquet.addLast(new Carte(12,Couleur.pique));

        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();
        paquet2.addLast(new Carte(12,Couleur.pique));

        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet);
        paquets.add(paquet2);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        JeuBataille partie = new JeuBataille(jdc);

        assertTrue(partie.round());

    }

    @Test
    void round_0win() throws Exception, CarteIncorrecteException {

        ArrayDeque<Carte> paquet = new ArrayDeque<>();
        paquet.addLast(new Carte(12,Couleur.pique));

        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();
        paquet2.addLast(new Carte(12,Couleur.pique));

        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet);
        paquets.add(paquet2);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        JeuBataille partie = new JeuBataille(jdc);

        assertEquals(-1,partie.numJoueurGagnant());

    }

    @Disabled
    @Test
    void testMalCompte(@Mock JeuDeCartes jdc) throws Exception, CarteIncorrecteException {
        ArrayDeque<Carte> paquet = new ArrayDeque<>();
        paquet.addLast(new Carte(12,Couleur.pique));
        paquet.addLast(new Carte(12,Couleur.pique));


        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();

        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet);
        paquets.add(paquet2);
        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        //assertThrows(RuntimeException.class , () -> new JeuBataille(jdc));
    }

    @Test
    void round_3cartes() throws Exception, CarteIncorrecteException {

        ArrayDeque<Carte> paquet = new ArrayDeque<>();
        paquet.addLast(new Carte(12,Couleur.pique));
        paquet.addLast(new Carte(1,Couleur.coeur));
        paquet.addLast(new Carte(4,Couleur.coeur));


        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();
        paquet2.addLast(new Carte(12,Couleur.pique));
        paquet2.addLast(new Carte(1,Couleur.coeur));
        paquet2.addLast(new Carte(1,Couleur.coeur));


        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet);
        paquets.add(paquet2);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        JeuBataille partie = new JeuBataille(jdc);


        Boolean result = partie.round();

        assertTrue(result);

    }

    @Test
   void Vanqueur1() throws CarteIncorrecteException, Exception {

        ArrayDeque<Carte> paquet1 = new ArrayDeque<>();
        paquet1.addLast(new Carte(12, Couleur.pique));
        paquet1.addLast(new Carte(5, Couleur.coeur));

        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();
        paquet2.addLast(new Carte(12, Couleur.pique));
        paquet2.addLast(new Carte(3, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet1);
        paquets.add(paquet2);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        JeuBataille partie = new JeuBataille(jdc);
        partie.round();
        assertEquals(1,partie.numJoueurGagnant());


    }


    @Test
    void TestTaillJoeur1() throws CarteIncorrecteException, Exception {
        ArrayDeque<Carte> paquet1 = new ArrayDeque<>();
        paquet1.addLast(new Carte(12, Couleur.pique));
        paquet1.addLast(new Carte(5, Couleur.coeur));

        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();
        paquet2.addLast(new Carte(12, Couleur.pique));
        paquet2.addLast(new Carte(3, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet1);
        paquets.add(paquet2);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        JeuBataille partie = new JeuBataille(jdc);
        assertEquals(2,partie.tailleJeuJoueur1());

    }
    @Test
    void TestTaillJoeur2() throws CarteIncorrecteException, Exception {
        ArrayDeque<Carte> paquet1 = new ArrayDeque<>();
        paquet1.addLast(new Carte(12, Couleur.pique));
        paquet1.addLast(new Carte(5, Couleur.coeur));

        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();
        paquet2.addLast(new Carte(12, Couleur.pique));
        paquet2.addLast(new Carte(3, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet1);
        paquets.add(paquet2);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        JeuBataille partie = new JeuBataille(jdc);
        assertEquals(2,partie.tailleJeuJoueur2());

    }

    @Test
    void JeuFiniTest() throws CarteIncorrecteException, Exception {

        ArrayDeque<Carte> paquet1 = new ArrayDeque<>();
        paquet1.addLast(new Carte(12, Couleur.pique));
        paquet1.addLast(new Carte(5, Couleur.coeur));

        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();
        paquet2.addLast(new Carte(12, Couleur.pique));
        paquet2.addLast(new Carte(3, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet1);
        paquets.add(paquet2);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        JeuBataille partie = new JeuBataille(jdc);
        partie.round();
        assertTrue(partie.jeuFini());

    }
    @Test
    void Vanqueur2() throws CarteIncorrecteException, Exception {

        ArrayDeque<Carte> paquet1 = new ArrayDeque<>();
        paquet1.addLast(new Carte(12, Couleur.pique));
        paquet1.addLast(new Carte(5, Couleur.coeur));

        ArrayDeque<Carte> paquet2 = new ArrayDeque<>();
        paquet2.addLast(new Carte(12, Couleur.pique));
        paquet2.addLast(new Carte(7, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> paquets =  new ArrayList<>();
        paquets.add(paquet1);
        paquets.add(paquet2);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(paquets);

        JeuBataille partie = new JeuBataille(jdc);
        partie.round();
        assertEquals(2,partie.numJoueurGagnant());


    }
    // fin normale = un des deux jours n'a plus de carte
    // avant la fin normale y a un batille a chaque fois = les deux n'ont plus de carte
    @Test
    void Avant_fin_normale() throws CarteIncorrecteException, Exception {


        ArrayDeque<Carte> paquet01 = new ArrayDeque<>();
        paquet01.addLast(new Carte(13, Couleur.trefle));


        ArrayDeque<Carte> paquet02 = new ArrayDeque<>();
        paquet02.addLast(new Carte(13, Couleur.pique));

        ArrayList<ArrayDeque<Carte>> cartesjouees =  new ArrayList<>();
        cartesjouees.add(paquet01);
        cartesjouees.add(paquet02);

        when(jdc.distribution(anyInt(),anyInt())).thenReturn(cartesjouees);

        JeuBataille partie = new JeuBataille(jdc);
        Boolean jeufini = partie.round();
        assertTrue(jeufini);
    }



/*
    @Test
    void round_2cartes() throws CarteIncorrecteException {
        when (deque.removeFirst()).thenReturn(new Carte());
        assertEquals(jb.round(),false);

    }
*/

}